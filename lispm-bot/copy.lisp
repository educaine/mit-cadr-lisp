;;; -*- Mode: Lisp; Base: 8; Package: brad -*-

;; (DEFVAR *LOGICAL-DIRECTORIES*
;;   '(("CHAOS" :NEVER T)  ; we don't need the host table
;;     ("DISTRIBUTION")
;;     ("LCADR")
;;     ("LISPM")
;;     ("LISPM1")
;;     ("LISPM2")
;;     ("LMCONS")
;;     ("LMDEMO")
;;     ("LMFONTS")
;;     ("LMFS")
;;     ("LMIO")
;;     ("LMIO1")
;;     ("LMPAT")
;;     ("LMWIN")
;;     ("LMSITE")
;;     ("ZMAIL")
;;     ("ZWEI"))
;;   "An alist of SYS logical directories and how they should be copied.")

;;(setq *LOGICAL-DIRECTORIES*
;;  '(("LMSITE")))

(defvar *LOGICAL-DIRECTORIES* nil)

(setq *LOGICAL-DIRECTORIES*
  '(("CHAOS")
    ("DISTRIBUTION")
    ("LCADR")
    ("LISPM")
    ("LISPM1")
    ("LISPM2")
    ("lispm-bot")
    ("LMCONS")
    ("LMDEMO")
    ("LMFONTS")
    ("LMFS")
    ("LMIO")
    ("LMIO1")
    ("LMPAT")
    ("LMWIN")
    ("LMSITE")
    ("ubin")
    ("ZMAIL")
    ("ZWEI")))

;;----------------------------------------------------------------

(DEFVAR COPY-LOSSES ())
(DEFVAR COPY-OVERS ())

;renamed from COPY-FILE to avoid conflict with system function.
(DEFUN FS-COPY-FILE (FROM TO &KEY &OPTIONAL OVERWRITE (VERBOSE T) DELETE AFTER
				    DIRECTORY-LIST DEFAULT-BYTE-SIZE
				    OUTPUT-DIRECTORY-LIST (CREATE-DIRECTORY T)
		     &ALLOW-OTHER-KEYS &AUX
					 TRUE-BYTE-SIZE KNOWN-BYTE-SIZE TRUE-CHARACTERS
					 TRUENAME OUTNAME OUTNAME-UNCERTAIN
					 TYPE QFASLP INSTREAM OUTSTREAM (ABORT-FLAG ':ABORT)
					 FROM-IS-STREAM-P TO-DEFAULTS-FROM-STREAM AUTHOR TEM)
  ;;(format t "fs-copy-file: from ~S to ~S~%" from to)
  (*CATCH 'COPY-FILE
	  (UNWIND-PROTECT
	       (PROG ()
		  (COND ((STRINGP FROM))
			((TYPEP FROM 'fs:PATHNAME))
			((SI:IO-STREAM-P FROM)
			 (SETQ FROM-IS-STREAM-P T)
			 (SETQ INSTREAM FROM)))

		  (IF (NULL FROM-IS-STREAM-P)
		      (SETQ FROM (fs:MERGE-PATHNAME-DEFAULTS FROM NIL ':UNSPECIFIC ':UNSPECIFIC)))

		  ;; If possible, get the byte size from the directory info.
		  (COND (DIRECTORY-LIST
			 (IF (NULL FROM-IS-STREAM-P) (SETQ TRUENAME FROM))
			 (SETF (VALUES OUTNAME OUTNAME-UNCERTAIN TO-DEFAULTS-FROM-STREAM)
			       (DETERMINE-COPY-DESTINATION TO TRUENAME NIL INSTREAM))
			 ;; Punt now if :AFTER specification is not met.
			 (AND AFTER
			      ( (GET (LOCF DIRECTORY-LIST) ':CREATION-DATE)
				  AFTER)
			      (RETURN ':AFTER))
			 ;; Verbosify after calling DETERMINE-COPY-DESTINATION.
			 (IF VERBOSE (FORMAT T "~%~A~23T ~A~50T"
					     (IF FROM-IS-STREAM-P "" TRUENAME) OUTNAME))
			 ;; If we are sure we know the destination name,
			 ;; and we have an output directory list, check now
			 ;; in case we don't need to copy.
			 (OR TO-DEFAULTS-FROM-STREAM
			     OUTNAME-UNCERTAIN
			     (AND OUTPUT-DIRECTORY-LIST
				  (LET ((DESTEX (COPY-DESTINATION-EXISTS-P
						 OVERWRITE OUTPUT-DIRECTORY-LIST
						 OUTNAME TRUENAME VERBOSE
						 (OR (GET (LOCF DIRECTORY-LIST) ':CREATION-DATE)
						     (FUNCALL INSTREAM ':CREATION-DATE)))))
				    (AND DESTEX (RETURN DESTEX)))))
			 (LET ((CHRLOC (LOCF (GET (LOCF DIRECTORY-LIST) ':CHARACTERS))))
			   (AND CHRLOC (SETQ TRUE-CHARACTERS (CDR CHRLOC))))
			 ;; Take :DIRECTORY-LIST information with a grain of salt...
			 ;; Note that we are assuming here that the files are used for LISPMs...
			 (LET ((POSSIBLE-BYTE-SIZE (GET (LOCF DIRECTORY-LIST) ':BYTE-SIZE)))
			   (AND POSSIBLE-BYTE-SIZE
				(COND ((EQ POSSIBLE-BYTE-SIZE 7.)
				       (SETQ TRUE-BYTE-SIZE 8.))
				      ((NEQ POSSIBLE-BYTE-SIZE 36.)
				       (SETQ TRUE-BYTE-SIZE POSSIBLE-BYTE-SIZE)))))))

		  ;;(format t ":type ~A~%" (funcall outname ':TYPE))
		  (if (equal (funcall from ':TYPE) "QFASL")
		      (progn
			(setq true-byte-size 16.)
			(setq true-characters nil)))

		  ;; Next try opening the file.
		  (COND ((NULL FROM-IS-STREAM-P)
			 (SETQ INSTREAM (OPEN FROM ':CHARACTERS (OR TRUE-CHARACTERS ':DEFAULT)
					      ':BYTE-SIZE (OR TRUE-BYTE-SIZE ':DEFAULT)
					      ':ERROR NIL))
			 (COND ((ERRORP INSTREAM)
				(AND VERBOSE (FORMAT T "~%~A~50T~A" FROM INSTREAM))
				(RETURN INSTREAM)))))

		  ;; Punt now if :AFTER specification is not met.
		  (AND AFTER
		       ( (OR (GET (LOCF DIRECTORY-LIST) ':CREATION-DATE)
			       (FUNCALL INSTREAM ':CREATION-DATE)
			       (FERROR NIL "Bletch!!"))
			   AFTER)
		       (RETURN ':AFTER))

		  (IF (NULL FROM-IS-STREAM-P)
		      (SETQ TRUENAME (FUNCALL INSTREAM ':TRUENAME)))
		  (SETQ QFASLP (FUNCALL INSTREAM ':QFASLP))

		  ;; Now determine the destination if not done already.
		  (IF (OR (NULL OUTNAME) OUTNAME-UNCERTAIN)
		      (PROGN
			(MULTIPLE-VALUE (OUTNAME TEM TO-DEFAULTS-FROM-STREAM)
					(DETERMINE-COPY-DESTINATION TO TRUENAME QFASLP INSTREAM))))
		  ;;		  (AND VERBOSE (FORMAT T "~%(2) ~A~23T ~A~50T" TRUENAME OUTNAME))))

		  ;; Does the output file already exist?  Is its date the same?
		  ;; Check now if we didn't check before.
		  (AND (NULL TO-DEFAULTS-FROM-STREAM)
		       (OR OUTNAME-UNCERTAIN (NOT OUTPUT-DIRECTORY-LIST))
		       (LET ((DESTEX (COPY-DESTINATION-EXISTS-P
				      OVERWRITE OUTPUT-DIRECTORY-LIST OUTNAME TRUENAME VERBOSE
				      (OR (GET (LOCF DIRECTORY-LIST) ':CREATION-DATE)
					  (FUNCALL INSTREAM ':CREATION-DATE)))))
			 (AND DESTEX (RETURN DESTEX))))

		  ;; If we knew the byte size before opening the stream, remember that fact.
		  (SETQ KNOWN-BYTE-SIZE TRUE-BYTE-SIZE)
		  (SETQ TYPE (IF FROM-IS-STREAM-P
				 (FUNCALL INSTREAM ':TYPE)
				 (FUNCALL TRUENAME ':TYPE)))
		  (OR TRUE-BYTE-SIZE
		      ;; If stream knows its proper byte size, believe it.  QFILE streams don't.
		      (AND (SETQ TRUE-BYTE-SIZE (FUNCALL INSTREAM ':SEND-IF-HANDLES ':BYTE-SIZE))
			   ;; If it knows that, it also did :characters :default properly.
			   (PROGN (SETQ TRUE-CHARACTERS (FUNCALL INSTREAM ':CHARACTERS)) T))
		      ;; Otherwise guess.
		      (SETQ TRUE-BYTE-SIZE
			    (COND (QFASLP 16.)
				  ((MEMBER TYPE BINARY-FILE-TYPES)
				   16.)
				  ((MEMBER TYPE PDP10-FILE-TYPES)
				   9)
				  ((FILE-EXTRACT-ATTRIBUTE-LIST INSTREAM)
				   8)
				  ((OR (MEMQ TYPE '(NIL :UNSPECIFIC))
				       (MEMBER TYPE CHARACTER-FILE-TYPES))
				   8)
				  (DEFAULT-BYTE-SIZE)
				  ((Y-OR-N-P (FORMAT NIL "~%Is ~A a CHARACTER File? " TRUENAME))
				   8)
				  (T 16.))))
		  ;;(format t ":type ~A~%" (funcall outname ':TYPE))
		  (if (equal (funcall outname ':TYPE) "QFASL")
		      (setq true-byte-size 16.))

		  (OR TRUE-CHARACTERS
		      (SETQ TRUE-CHARACTERS (= TRUE-BYTE-SIZE 8)))
		  (FORMAT T " Byte size ~D, Characters ~S" TRUE-BYTE-SIZE TRUE-CHARACTERS)

		  ;; If stream is open in wrong byte size or with wrong :characters, reopen it.
		  ;; 	    (OR FROM-IS-STREAM-P
		  ;; 		(AND
		  ;; 		  (OR KNOWN-BYTE-SIZE
		  ;; 		      (= TRUE-BYTE-SIZE
		  ;; 			 (OR (FUNCALL INSTREAM ':SEND-IF-HANDLES ':BYTE-SIZE)
		  ;; 			     (IF (FUNCALL INSTREAM ':CHARACTERS) 8 16.))))
		  ;; 		  (EQ TRUE-CHARACTERS (FUNCALL INSTREAM ':CHARACTERS)))
		  ;; 		(PROGN (PRINC " -- Must reopen stream")
		  ;; 		       (CLOSE INSTREAM)
		  ;; 		       (SETQ INSTREAM (OPEN TRUENAME ':ERROR NIL
		  ;; 					   ':BYTE-SIZE TRUE-BYTE-SIZE
		  ;; 					   ':CHARACTERS (= TRUE-BYTE-SIZE 8)))
		  ;; 		       (COND ((ERRORP INSTREAM)
		  ;; 			      (AND VERBOSE (FORMAT T "~%~A~50T~A" FROM INSTREAM))
		  ;; 			      (RETURN INSTREAM)))))

		  (SETQ AUTHOR
			(OR (GET (LOCF DIRECTORY-LIST) ':AUTHOR)
			    (FUNCALL INSTREAM ':GET ':AUTHOR)
			    (IF (NULL FROM-IS-STREAM-P)
				(DETERMINE-FILE-AUTHOR (FUNCALL INSTREAM ':TRUENAME)))
			    "Unknown"))
		  OPEN-OUTPUT
		  ;; 	    ;; Do It.
		  ;; 	    (CONDITION-CASE (ERROR)
		  ;; 		(SETQ OUTSTREAM
		  ;; 		      (COND (TO-DEFAULTS-FROM-STREAM
		  ;; 			     (FUNCALL OUTNAME ':OPEN OUTNAME
		  ;; 				      ':DIRECTION ':OUTPUT
		  ;; 				      ':ERROR NIL
		  ;; 				      ':BYTE-SIZE TRUE-BYTE-SIZE
		  ;; 				      ':DEFAULTS-FROM-STREAM INSTREAM
		  ;; 				      ':AUTHOR AUTHOR))
		  ;; 			    (T
		  ;; 			     (OPEN OUTNAME
		  ;; 				   (COND ((EQ TRUE-BYTE-SIZE 8.)
		  ;; 					  '(:WRITE :NOERROR))
		  ;; 					 ((EQ TRUE-BYTE-SIZE 16.)
		  ;; 					  '(:WRITE :FIXNUM :NOERROR))
		  ;; 					 (T `(:WRITE :NOERROR
		  ;; 						     :BYTE-SIZE ,TRUE-BYTE-SIZE)))))))
		  ;; 	      (DIRECTORY-NOT-FOUND
		  ;; 	       (AND CREATE-DIRECTORY
		  ;; 		    (NOT (ERRORP (CREATE-DIRECTORY OUTNAME ':ERROR NIL)))
		  ;; 		    (GO OPEN-OUTPUT))
		  ;; 		   (AND VERBOSE (FUNCALL STANDARD-OUTPUT ':STRING-OUT ERROR))
		  ;; 		   (RETURN OUTSTREAM))
		  ;; 	      (ERROR
		  ;; 	       (AND VERBOSE (FUNCALL STANDARD-OUTPUT ':STRING-OUT ERROR))
		  ;; 	       (RETURN OUTSTREAM)))
		  ;; (format t "open-done~%")

		  ;;(format t "calling open on ~A~%" outname)
		  (unwind-protect
		       (SETQ OUTSTREAM
			     (OPEN OUTNAME
				   (COND ((EQ TRUE-BYTE-SIZE 8.)
					  '(:WRITE :NOERROR))
					 ((EQ TRUE-BYTE-SIZE 16.)
					  '(:WRITE :FIXNUM :NOERROR))
					 (T `(:WRITE :NOERROR :BYTE-SIZE TRUE-BYTE-SIZE)))))
		    (progn
		      (cond ((and (errorp outstream) (stringp outstream))
			     (format t "error: ~A~%" outstream)
			     (setq start (1+ (string-search-char #\sp outstream)))
			     (setq start (1+ (string-search-char #\sp outstream start)))
			     (setq end (or (string-search-set '(#\sp #\cr) outstream start)
					   (string-length returned-string)))
			     (setq error-code (nsubstring outstream start end))
					;(format t "error-code '~A'~%" error-code)
					;(format t "outname ~A~%" outname)
					;(format t "outname typep ~A~%" (typep outname))
					;(format t "outname host ~A~%" (funcall outname ':HOST))
					;(format t "outname directory ~A~%" (funcall outname ':DIRECTORY))
			     (if (equal error-code "NSD")
				 (progn
				   (setq pn (fs:parse-pathname outname))
				   (format t "creating dir ~S~%" outname);
				   (lmfs:send pn ':CREATE-DIRECTORY)
				   (GO OPEN-OUTPUT))
				 ;; 	       (AND CREATE-DIRECTORY
				 ;; 		    (NOT (ERRORP (CREATE-DIRECTORY OUTNAME ':ERROR NIL)))
				 ;; 		    (GO OPEN-OUTPUT))
;;;; 		   (AND VERBOSE (FUNCALL STANDARD-OUTPUT ':STRING-OUT ERROR))
				 (format t "create-directory failed~%")
				 (RETURN OUTSTREAM))
			     ))
		      )
		    )
		  ;;(format t "outstream ~A~%" outstream)

		  ;; This now hacks arbitrary property stuff...
					; 	    (IF TO-DEFAULTS-FROM-STREAM NIL
					; 		(FUNCALL OUTSTREAM ':CHANGE-PROPERTIES NIL
					; 			 ':AUTHOR AUTHOR
					; 			 ':CREATION-DATE (FUNCALL INSTREAM ':GET ':CREATION-DATE)))

		  (fs:change-file-properties outname nil
					     ':CREATION-DATE (FUNCALL INSTREAM ':GET ':CREATION-DATE))

		  ;; 		(LOOP WITH other-properties = (or directory-list
		  ;; 						  (funcall instream ':plist))
		  ;; 		      AS remove-properties = (funcall outstream ':plist)
		  ;; 		      THEN (cddr remove-properties)
		  ;; 		      WHILE (and remove-properties other-properties)
		  ;; 		      DO
		  ;; 		      (remprop (locf other-properties) (car remove-properties))
		  ;; 		      FINALLY
		  ;; 		      (dolist (p '(:directory :name :version :type))
		  ;; 			(remprop (locf other-properties) p))
		  ;; 		      FINALLY
		  ;; 		      (cond (other-properties
		  ;; 			     (lexpr-funcall outstream ':CHANGE-PROPERTIES NIL
		  ;; 					    other-properties)))))
		  (STREAM-COPY-UNTIL-EOF INSTREAM OUTSTREAM NIL)
		  (SETQ ABORT-FLAG NIL))

	    (OR (NULL OUTSTREAM) (ERRORP OUTSTREAM)
		(FUNCALL OUTSTREAM ':CLOSE ABORT-FLAG))
	    (OR (NULL INSTREAM) (ERRORP INSTREAM)
		(PROGN (AND (NOT ABORT-FLAG)
			    DELETE
			    (FUNCALL INSTREAM ':SEND-IF-HANDLES ':DELETE NIL))
		       (FUNCALL INSTREAM ':CLOSE ABORT-FLAG))))))

(DEFUN DETERMINE-COPY-DESTINATION (TO TRUENAME QFASLP INSTREAM)
;;(format t "determine-copy-destination to ~S~%" to)
  (LET (TYPE VERSION PLIST TEM NOT-CERTAIN)
    (AND (SYMBOLP TO)
	 (SETQ TO (GET-PNAME TO)))
    (COND ((TYPEP TO 'fs:PATHNAME))
	  ((CAR (ERRSET (GET-PATHNAME-HOST TO NIL NIL) NIL))
	   (SETQ TO (fs:MAKE-PATHNAME ':HOST TO ':DIRECTORY NIL ':NAME NIL)))
	  (T (SETQ TO (fs:PARSE-PATHNAME TO))))
    (COND ((NOT (TYPEP TO 'fs:PATHNAME))
	   (VALUES TO NIL 'DEFAULTS-FROM-STREAM))
	  ((NULL TRUENAME)
	   (LET ((NAME (FUNCALL TO ':NAME)))
	     (COND ((NULL NAME)
		    (SETQ NAME (FUNCALL INSTREAM ':NAME))
		    (AND (LISTP NAME) (NULL (CDR NAME))
			 (SETQ NAME (CAR NAME)))))
	     (COND ((MEMQ (SETQ TYPE (FUNCALL TO ':TYPE)) '(NIL :UNSPECIFIC))
		    (SETQ TYPE (FUNCALL INSTREAM ':TYPE))))
	     (COND ((SYMBOLP (SETQ VERSION (FUNCALL TO ':VERSION)))
		    (SETQ VERSION (FUNCALL INSTREAM ':VERSION))))
	     (VALUES
	       (fs:MAKE-PATHNAME ':HOST (FUNCALL TO ':HOST)
			      ':DEVICE (OR (FUNCALL TO ':DEVICE)
					   (FUNCALL INSTREAM ':DEVICE))
			      ':DIRECTORY (OR (FUNCALL TO ':DIRECTORY)
					      (FUNCALL INSTREAM ':DIRECTORY))
			      ':NAME NAME
			      ':TYPE TYPE
			      ':VERSION VERSION)
	       NIL)))
	  (T
	   (OR (NOT (MEMQ (SETQ TYPE (FUNCALL TO ':TYPE)) '(NIL :UNSPECIFIC)))
	       (NOT (MEMQ (SETQ TYPE (FUNCALL TRUENAME ':TYPE)) '(NIL :UNSPECIFIC)))
	       ;; These do not distinguish types LISP and TEXT.
	       (TYPEP TO 'fs:ITS-PATHNAME)
	       (AND (GET 'LOCAL-FILE-PATHNAME 'SI:FLAVOR)
		    (TYPEP TO 'fs:LOCAL-FILE-PATHNAME))
	       (AND (GET 'REMOTE-LMFILE-PATHNAME 'SI:FLAVOR)
		    (TYPEP TO 'fs:REMOTE-LMFILE-PATHNAME))
	       (SETQ NOT-CERTAIN T
		     TYPE
		     (OR (AND QFASLP "QFASL")
			 (AND INSTREAM
			      (SETQ PLIST (FILE-READ-ATTRIBUTE-LIST NIL INSTREAM))
			      (EQ (GET (LOCF PLIST) ':MODE) ':LISP)
			      "LISP")
			 "TEXT")))
	   (OR (NOT (SYMBOLP (SETQ VERSION (FUNCALL TO ':VERSION))))
	       (NOT (SYMBOLP (SETQ VERSION (FUNCALL TRUENAME ':VERSION))))
	       (COND (QFASLP
		      (SETQ PLIST (SI:QFASL-STREAM-Property-List INSTREAM))
		      (FUNCALL INSTREAM ':SET-POINTER 0)
		      (COND ((SETQ TEM (GET (LOCF PLIST) ':QFASL-SOURCE-FILE-UNIQUE-ID))
			     (IF (LISTP TEM)
				 (SETQ VERSION (CAR (LAST TEM)))
			       (SETQ VERSION (FUNCALL TEM ':VERSION))))
			    (T (SETQ VERSION ':NEWEST))))
		     (T (SETQ NOT-CERTAIN T VERSION ':NEWEST))))
;(format t "truename ~A~%" TRUENAME)
;(format t "truename name ~A~%" (FUNCALL TRUENAME ':NAME))
;(format t "truename host ~A~%" (FUNCALL TRUENAME ':HOST))
;(format t "truename directory ~A~%" (FUNCALL TRUENAME ':DIRECTORY))
;(format t "to ~A~%" TO)
;(format t "to host ~A~%" (FUNCALL TO ':HOST))
;(format t "to directory ~A~%" (FUNCALL TO ':DIRECTORY))
;(format t "to name ~A~%" (FUNCALL TO ':NAME))
;(format t "type ~A~%" type)
;(format t "version ~A~%" version)
	   (LET ((INNAME (FUNCALL TRUENAME ':NAME)))
;(format t "before inname ~A~%" inname)
	     (AND (LISTP INNAME) (NULL (CDR INNAME))
		  (SETQ INNAME (CAR INNAME)))
;(format t "after inname ~A~%" inname)
	     (VALUES
	       (fs:MAKE-PATHNAME ':HOST (FUNCALL TO ':HOST)
			      ':DEVICE (OR (FUNCALL TO ':DEVICE)
					   (FUNCALL TRUENAME ':DEVICE))
			      ':DIRECTORY (OR (FUNCALL TO ':DIRECTORY)
					      (FUNCALL TRUENAME ':DIRECTORY))
			      ':NAME (OR (FUNCALL TO ':NAME)
					 INNAME)
			      ':TYPE TYPE
			      ':VERSION VERSION)
	       NOT-CERTAIN))))))

(DEFUN COPY-DIRECTORY (DIR TO
		       &OPTIONAL &REST OPTIONS
		       &KEY
		       COPY-ONLY
		       SELECTIVE
		       (RECOPY-FILE-ON-EOT T)
		       (ONLY-LATEST NIL)
		       (SINCE NIL)
		       (copy-subdirectories t)
		       &ALLOW-OTHER-KEYS
		       &AUX WHOLE-DIR-LIST OUTPUT-DIR-LIST ODIR TAPE-DUMP)
  (SETQ DIR (fs:PARSE-PATHNAME DIR))
;;   (SETQ DIR (FUNCALL DIR ':NEW-PATHNAME
;; 		     ':NAME (IF (MEMQ (FUNCALL DIR ':NAME) '(NIL :UNSPECIFIC))
;; 				':WILD
;; 				(FUNCALL DIR ':NAME))
;; 		     ':TYPE (IF (MEMQ (FUNCALL DIR ':TYPE) '(NIL :UNSPECIFIC))
;; 				':WILD
;; 				(FUNCALL DIR ':TYPE))
;; 		     ':VERSION (IF (MEMQ (FUNCALL DIR ':VERSION) '(NIL :UNSPECIFIC))
;; 				   ':WILD
;; 				   (FUNCALL DIR ':VERSION))))
  (SETQ ODIR (fs:PARSE-PATHNAME TO))
;;(format t "before odir ~A~%" odir)
  (COND ((TYPEP ODIR 'fs:PATHNAME)
	 (SETQ ODIR (FUNCALL ODIR ':NEW-PATHNAME
		      ':directory (if (memq (funcall odir ':directory)
					    '(nil :unspecific))
				      (funcall dir ':directory)
				    (funcall odir ':directory))
		      ':NAME (IF (MEMQ (FUNCALL ODIR ':NAME) '(NIL :UNSPECIFIC))
				 ':WILD
			       (FUNCALL ODIR ':NAME))
		      ':TYPE (IF (MEMQ (FUNCALL ODIR ':TYPE) '(NIL :UNSPECIFIC))
				 ':WILD
			       (FUNCALL ODIR ':TYPE))
		      ':VERSION (IF (MEMQ (FUNCALL ODIR ':VERSION) '(NIL :UNSPECIFIC :NEWEST))
				    ':WILD
				  (FUNCALL ODIR ':VERSION)))))
	((TYPEP ODIR 'MT-FILEHANDLE)
	 (SETQ TAPE-DUMP T)))
;;(format t "after odir ~A~%" odir)
  (SETQ WHOLE-DIR-LIST (fs:DIRECTORY-LIST DIR))
  (IF ONLY-LATEST (SETQ WHOLE-DIR-LIST
			(ELIMINATE-OLDER-FILES WHOLE-DIR-LIST)))
  (COND ((TYPEP ODIR 'fs:PATHNAME)
	 (ERRSET (SETQ OUTPUT-DIR-LIST (DIRECTORY-LIST ODIR))
		 NIL)))
  (DOLIST (F (IF SINCE (ELIMINATE-DATED-FILES (TIME:PARSE-UNIVERSAL-TIME SINCE)
					      WHOLE-DIR-LIST) WHOLE-DIR-LIST))
    (AND (CAR F)
	 (NOT (GET F ':LINK-TO))
	 (OR (NOT SELECTIVE)
	     (PROGN (FORMAT QUERY-IO "Copy ~A ?" (CAR F))
		    (Y-OR-N-P)))
;;	 (OR (NULL COPY-ONLY)
;;	     (CHECK-COPY-ONLY (CAR F) COPY-ONLY WHOLE-DIR-LIST))
	 (cond ((get f ':directory)
		(cond (copy-subdirectories
		       (let ((d (funcall (car f) ':directory)))
			 (cond ((not (listp d)) (setq d (list d))))
			 (lexpr-funcall #'brad:copy-directory
					(funcall (car f) ':new-pathname
						 ':directory (append d
								     (list (funcall (car f)
										    ':name)))
						 ':name ':wild
						 ':type ':wild
						 ':version ':wild)
						;to
;					(format t "[f = ~A] [d = ~A]" f d) (break t)
					(format nil "~a.~a;"
						(substring to 0 (string-search ";" to))
						(funcall (car f) ':name))
					options)))))
	       (t

	 (PROGV (IF RECOPY-FILE-ON-EOT '(*MT-EOT-HANDLER*) NIL)
		'(COPY-EOT-HANDLER)
	   (DO ((V) (EOT))
	       (())
	     (MULTIPLE-VALUE (V EOT)
	       (*CATCH (IF RECOPY-FILE-ON-EOT ':EOT ':NEVER)
		 (LEXPR-FUNCALL #'brad:FS-COPY-FILE (CAR F) TO ':DIRECTORY-LIST (CDR F)
				':OUTPUT-DIRECTORY-LIST OUTPUT-DIR-LIST OPTIONS)))
	     (IF (NULL EOT)
		 (RETURN)
		 (COPY-MOUNT-NEXT-TAPE TAPE-DUMP)))))))))


(DEFUN COPY-DESTINATION-EXISTS-P (OVERWRITE OUTPUT-DIRECTORY-LIST
				  OUTNAME TRUENAME VERBOSE INDATE)
  (LET (OUTPROBE)
    (AND (NEQ OVERWRITE ':ALWAYS)
	(LET (OUTCRDATE OUTEX)
	  ;; Take note of the fact that an LMFILE pathname with a "type"
	  ;; won't be found as a truename because the truename will have a space.
	  (AND (OR (AND (GET 'LOCAL-FILE-PATHNAME 'SI:FLAVOR)
			(TYPEP OUTNAME 'fs:LOCAL-FILE-PATHNAME))
		   (AND (GET 'REMOTE-LMFILE-PATHNAME 'SI:FLAVOR)
			(TYPEP OUTNAME 'fs:REMOTE-LMFILE-PATHNAME)))
	       (FUNCALL OUTNAME ':TYPE)
	       (SETQ OUTPUT-DIRECTORY-LIST NIL))
	  (IF OUTPUT-DIRECTORY-LIST
	      (PROGN (SETQ OUTEX (ASS #'(LAMBDA (X Y)
					  (AND Y
					       (STRING-EQUAL (FUNCALL X ':STRING-FOR-PRINTING)
							     (FUNCALL Y ':STRING-FOR-PRINTING))))
				      OUTNAME OUTPUT-DIRECTORY-LIST))
		     (SETQ OUTCRDATE (GET (LOCF (CDR OUTEX)) ':CREATION-DATE)))
	    (UNWIND-PROTECT
	     (PROGN
	      (SETQ OUTPROBE (OPEN OUTNAME '(:PROBE)))
	      (COND ((NOT (ERRORP OUTPROBE))
		     (SETQ OUTCRDATE (FUNCALL OUTPROBE ':CREATION-DATE))
		     (SETQ OUTEX T))))
	     (AND OUTPROBE (NOT (ERRORP OUTPROBE))
		  (CLOSE OUTPROBE))))
;(format t "outex ~A~%" outex)
;(format t "outcrdate ~A indate ~A~%" outcrdate indate)
	  (IF OUTEX
	      (IF (or (= OUTCRDATE INDATE) t)
		  (COND ((NOT OVERWRITE)
			 (AND VERBOSE (FUNCALL STANDARD-OUTPUT ':STRING-OUT
					       "[Already Exists]"))
			 ':ALREADY-EXISTS))
		  (COND ((NEQ (fs:PATHNAME-VERSION OUTNAME) ':NEWEST)
			 (AND VERBOSE (FUNCALL STANDARD-OUTPUT ':STRING-OUT
					       "[Different file exists at target]"))
			 (IF TRUENAME (PUSH TRUENAME COPY-LOSSES))
			 ':ALREADY-EXISTS)
			(T (IF TRUENAME (PUSH TRUENAME COPY-OVERS))
			   NIL))))))))

;;-------------------------------------------------------------

(DEFUN brad-test-copy (TO)
  (dolist (LDIR *LOGICAL-DIRECTORIES*)
    (FORMAT T "~%Working on the ~A directory.~%" (CAR LDIR))
    (SETQ TYPE ':ALL)
    (setq src-dir (string-downcase (string-append "files4//kansas//87//" (FIRST LDIR))))
    (format t "~&Would copy from: ~A" (FS:MAKE-PATHNAME ':HOST "server"
					  ':DIRECTORY src-dir
					  ':NAME "*"
					  ))))

(defun src-copy (to)
  (dolist (ldir *LOGICAL-DIRECTORIES*)
    (format t "~%Working on the ~A directory.~%" (car ldir))
    (setq type ':ALL)
    (setq src-dir (string-downcase (string-append "tree//" (first ldir))))
    (setq dst-path (string-append to ">" (first ldir) ">"))
;;(format t "src-dir ~A~%" src-dir)
;;(format t "dst-path ~A~%" dst-path)
    (brad:copy-directory (fs:make-pathname ':HOST "server"
				      ':DIRECTORY src-dir
				      ':NAME "*")
		    dst-path
		    ':copy-only ':all
		    ':only-latest nil
		    ':since nil
		    ':recopy-file-on-eot t
		    ':selective nil
		    ':copy-subdirectories ())))

(defun copy-src ()
  (unwind-protect
   (let ((tv:more-processing-global-enable nil))
    (src-copy "local:"))
   (setq tv:more-processing-global-enable t)))

;;; entry point for deleting all files and copying the contents of
;;; /tree.
(defun init-and-copy-src ()
  (unwind-protect
       (let ((tv:more-processing-global-enable nil))
	 (lmfs:fsmaint-fs-initialize)
	 (src-copy "local:"))
;;    (CREATE-DIRECTORY ">ubin" ':ERROR NIL)
    (setq tv:more-processing-global-enable t)))
