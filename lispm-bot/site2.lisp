;;;-*- Mode: LISP; Package: SYSTEM-INTERNALS; Base: 8 -*-

(DEFCONST SYS-HOST-DIRECTORY-TRANSLATIONS
  '(
    ("SYS" ">lispm>")
    ("SYS2" ">lispm2>")
    ("IO" ">lmio>")
    ("IO1" ">lmio1>")
    ("WINDOW" ">lmwin>")
    ("ZMAIL" ">zmail>")
    ("ZWEI" ">zwei>")
))


