#!/usr/bin/perl

my @files;

#open(F, 'find . -name "*.[0-9]*" -print |') || die "can't run find: $!";
open(F, 'find . -name "*;[0-9]*" -print |') || die "can't run find: $!";
@files = <F>;
close(F) || die "can't close find: $!";  

foreach $old (@files) {
    $old =~ s/[\r\n]*//g;
    $old =~ s/^\.\///g;
    print "$old -> ";
#    my ($a,$b,$c) = split(/\./, $old);
#    my $new = "$a.$b";
    my ($a,$b,$c) = split(/;/, $old);
    my $new = "$a";
    print "$new\n";
    #system("mv $old $new");
    rename($old, $new);
}

print "done\n";
exit 0;
