(defun begining-of-buffer ()
  (goto-char (point-min)))

(defun fixup ()
  (progn
    (save-excursion
      (begining-of-buffer)
      (query-replace "\214" "\014" nil nil nil)
      (begining-of-buffer)
      (query-replace "\211" "\11" nil nil nil)
      (begining-of-buffer)
      (query-replace "\215" "\12" nil nil nil))))

#(perform-replace from-string replacements start end nil nil nil)
