(defun setup-file-translations ()
  (fs:add-logical-pathname-host "sys" "local" si:sys-host-directory-translations))

(defun prep-build-locally ()
  (load "local:>lmsite>site.lisp")
  (load "local:>lmsite>hsttbl.lisp")
  (setup-file-translations)
  (si:set-sys-host "cadr" ':lispm 401)
  (setup-file-translations))


(defun build-locally (system)
  (prep-build-locally)
  (make-system system ':batch ':compile))

(defun make-self-unix-server ()
  (load "local:>lmsite>hsttbl.lisp")
  (si:set-sys-host "server" ':unix 404))


(defun assemble-microcode ()
  (build-locally 'micro-assembler)
  (ua:assemble))
