(defun setup-file-translations ()
  (fs:add-logical-pathname-host "sys" "cadr" SYS-HOST-DIRECTORY-TRANSLATIONS))

(defun build-locally ()
  (load ">lmsite>site.lisp")
  (load ">lmsite>hsttbl.lisp")
  (setup-file-translations)
  (si:sys-sys-host "cadr" ':lispm 401)
  (make-system 'batch 'zwei))
