;;; -*- Syntax: Common-Lisp;  Base: 10; Mode: LISP -*-

;; To get things going.
;; 1) create /tree in / so that /tree/earl/lisp.init is this file
;; 1) use File.sys99 (disk.template6) as the file partition.
;; 1) create earl folder using the file maintenance tool.
;; 2) Configure >site>hsttbl.lisp to have a cadr and server
;;    entry. leaving the rest of the entries is fine.
;; 3) (load "local:>site>hsttbl.lisp")
;; 4) (si:sys-sys-host "SERVER" ':unix 404)
;; 5) In Zemacs: M-x Copy Binary File <server:/tree/earl/lispm.init>
;;    <local:>earl>site-init2.lisp>
;;    Note the from and to base file names are different.
;; 6) Now you can load this file: (load "local:>earl>lisp-init2.lisp")


(defun get-value-list (key list)
  (if (or (not list)
	  (< (length list) 2))
      nil
    (if (eq key (car list)) 
	(cadr list)
      (get-value-list key (cddr list)))))

(defvar *directory-list* nil)

(if (not *directory-list*) 
    (setq *directory-list* (fs:directory-list "local:>")))

(defun directory-p (pathname)
  (get-value-list 'directory (fs:file-properties pathname)))

(defun run-directory-p ()
  (dolist (file *directory-list*)
    (directory-p (car file))))

(defun rm (dir)
  "TODO EEK -- add ordinary rm flags. recursive delete files and directory."
  (let ((files (fs:directory-list dir)))
    (dolist (file files)
      (format t "~s~%" file)
      (if (car file)
	  (deletef (car file))))))

(defun run-rm ()
  (rm "local:>lispm1>*.*"))

(defun com-copy-binary-file (from to)
  "Copy one binary file to another."
  (with-open-file (from-stream from '(:IN :FIXNUM))
    (with-open-file (to-stream to '(:OUT :FIXNUM))
      (stream-copy-until-eof from-stream to-stream nil))))


(defun prepare-remote ()
  (load "local:>lmsite>hsttbl.lisp")
  (si:set-sys-host "SERVER" ':unix 404)
  (load "SERVER://tree//earl//lispm.init"))


(defvar *font-files* '("43vxms.qfasl"
		     "5x5.qfasl"
		     "abacus.qfasl"
		     "bigfnt.qfasl"
		     "color-5x5.qfasl"
		     "color-cptfont.qfasl"
		     "color-medfnt.qfasl"
		     "color-mouse.qfasl"
		     "cptfontb.qfasl"
		     "cptfont.qfasl"
		     "hl10b.qfasl"
		     "hl10.qfasl"
		     "hl12bi.qfasl"
		     "hl12b.qfasl"
		     "hl12i.qfasl"
		     "hl12.qfasl"
		     "hl6.qfasl"
		     "hl7.qfasl"
		     "medfnb.qfasl"
		     "medfnt.qfasl"
		     "metsi.qfasl"
		     "mets.qfasl"
		     "mouse.qfasl"
		     "narrow.qfasl"
		     "search.qfasl"
		     "tiny.qfasl"
		     "tog.qfasl"
		     "tr10bi.qfasl"
		     "tr10b.qfasl"
		     "tr10i.qfasl"
		     "tr10.qfasl"
		     "tr12i.qfasl"
		     "tr8b.qfasl"
		     "tr8i.qfasl"
		     "tr8.qfasl"
		     "tvfont.qfasl"
		     "worm.qfasl"))

(defvar *to-file-suffix* "local:>fonts>")
(defvar *from-file-suffix* "server://tree//fonts//")

(defun file-exists (file)
  (with-open-file (s file ':noerror) (not (stringp s))))



(defun copy-font-files ()
  (dolist (file *font-files*)
    (let ((from-file (string-append *from-file-suffix* file))
	  (to-file (string-append *to-file-suffix* file)))
      (if (not (file-exists to-file))
	  (com-copy-binary-file from-file to-file)))))

(defun get-user-homedir ()
  (fs:user-homedir))

(defun ls ()
  (fs:directory-list))



(defun run-file-property-bindings ()
  (fs:file-property-bindings (ps:parse-pathname "local:>")))