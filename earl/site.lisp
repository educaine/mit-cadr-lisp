(DEFCONST SYS-HOST-DIRECTORY-TRANSLATIONS
  '(("CC" ">cc>")
    ("CHAOS" ">chaos>")
    ("COLD" ">cold>")
    ("DEMO" ">demo>")
    ("DISTRIBUTION" ">distribution>")
    ("DOC" ">doc>")
    ("FILE" ">FILE>")
    ("FILE2" ">FILE2>")
    ("FONTS" ">fonts>")
    ("IO" ">io>")
    ("IO1" ">io1>")
    ("MAN" ">man>")
    ("NETWORK" ">network>")
    ("PATCH" ">patch>")
    ("SITE" ">site>")
    ("SYS" ">sys>")
    ("SYS2" ">sys2>")
    ("TAPE" ">tape>")
    ("UBIN" ">ubin>")
    ("UCADR" ">ucadr>")
    ("WINDOW" ">window>")
    ("ZMAIL" ">zmail>")
    ("ZWEI" ">zwei>")
    ))

;;; Here is the DEFSITE special form.

(DEFSITE :DISTRIBUTION
  (:CHAOS-FILE-SERVER-HOSTS '("cadr"))
  (:CHAOS-HOST-TABLE-SERVER-HOSTS '("cadr"))
  (:CHAOS-MAIL-SERVER-HOSTS '("cadr"))
  (:CHAOS-TAPE-SERVER-HOSTS '("cadr"))
  (:CHAOS-TIME-SERVER-HOSTS '("cadr"))
  (:CHAOS T)
  (:DEFAULT-MAIL-MODE ':CHAOS)
  (:ESC-F-ARG-ALIST '(
		      ;; TERMINAL F lists the users on the default file
		      ;; host (usually the VAX, but see the Lisp Machine
		      ;; Manual's description of the LOGIN function,
		      ;; p. 506).
		      (NIL . :LOGIN)
		      ;; TERMINAL 1 F lists the LM-2 users.
		      (1 . :LOCAL-LISP-MACHINES)
		      ;; TERMINAL 2 F
		      (2 . ("cadr"))
		      ;; TERMINAL 0 F prompts for which users to list.
		      (0 . :READ)))

  (:HOST-FOR-BUG-REPORTS "cadr")
  (:LISPM-FILE-SERVERS T)
  (:LOCAL-MAIL-HOSTS '("cadr"))
  (:SITE-PRETTY-NAME "local")
  (:SYS-HOST "cadr")
  (:SYS-DIRECTORY-TRANSLATIONS SYS-HOST-DIRECTORY-TRANSLATIONS)
  (:TIMEZONE 5)
  ;; End of DEFSITE.
  )
